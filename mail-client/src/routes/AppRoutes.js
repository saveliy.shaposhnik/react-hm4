import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import Cart from '../pages/Cart/Cart';
import Main from '../pages/Main/Main';
import Favourites from "../pages/Favourite/Favourite"


export default function AppRoutes(props) {
    const {setItems,setLiked,openModal,items} = props;
    return (
        <div className="app-routes">
            <Switch>
                <Redirect exact from="/" to="/main" />
                <Route exact path="/main" render={() => <Main setItems={setItems} setLiked={setLiked} onClick={openModal} items={items}/>} />
                <Route exact path="/cart" render={() => <Cart setItems={setItems} setLiked={setLiked} onClick={openModal} items={items}/>} />
                <Route exact path="/favourites" render={() => <Favourites setItems={setItems} setLiked={setLiked} onClick={openModal} items={items}/>} />
                <Route path="*" render={() => <div>404</div>} />
            </Switch>
        </div>
    )
}
