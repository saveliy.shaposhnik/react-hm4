import { combineReducers } from "redux";
import itemsReducer from "./items/reducer"
import modalReducer from "./modal/reducer"


const reducer = combineReducers({
    modal: modalReducer,
    items: itemsReducer,
})

export default reducer;