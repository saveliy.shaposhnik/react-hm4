import { LOAD_ITEMS_REQUEST, LOAD_ITEMS_SUCCESS } from "./types"
import axios from "axios"

export const getItems = () => (dispatch) => {
    dispatch({ type: LOAD_ITEMS_REQUEST, payload: true })
    axios('/items.json').then((res) => {
        const favouriteInLocal = JSON.parse(localStorage.getItem("Liked")) || [];
        const cartInLocal = JSON.parse(localStorage.getItem("Cart")) || [];
        const newArr = res.data.map(el => {
            if (favouriteInLocal.includes(el.id)) {
                el.isFavorite = !el.isFavorite
            }
            if(cartInLocal.includes(el.id)){
                el.inCart = !el.inCart
            }
            return el
        })
        dispatch({ type: LOAD_ITEMS_SUCCESS, payload: newArr })
    })
}