import React, { useState } from 'react'
import Items from '../../components/Items/Items';
import "./Main.scss"

export default function Main(props) {
    
    const { onClick, setLiked,items } = props;
    const [isMainOpen] = useState(true)

    return (
        <div className="cards-div">
            {items.map(el => {
                return <Items key={el.id} items={el} setLiked={setLiked} onClick={onClick} isMainOpen={isMainOpen}/>
            })}
        </div>
    )
}
