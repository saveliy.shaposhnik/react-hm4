import React,{useState} from 'react'
import Items from '../../components/Items/Items';
import NoItems from '../../components/NoItems/NoItems';

export default function Cart(props) {

    const { items, setLiked, onClick } = props;
    const [isCartOpen] = useState(true)
    const cart = JSON.parse(localStorage.getItem("Cart")) || []
    let cartItems = []

    const getLocalCart = () => {
        items.forEach(el => {
            cart.forEach(element => {
                if (element === el.id) {
                    cartItems.push(el);
                }
            });
        })
    }
    if(cart.length === 0){
        return <NoItems/>
    }
    return (
        <div className="cards-div">
            {getLocalCart()}
            {cartItems.map(el => {
                return <Items key={el.id} items={el} setLiked={setLiked} onClick={onClick} isCartOpen={isCartOpen}/>
            })}
        </div>
    )
}
