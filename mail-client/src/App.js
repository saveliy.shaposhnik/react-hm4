import './App.css';
import Modal from './components/Modal/Modal';
import React, { useEffect } from 'react'
import AppRoutes from './routes/AppRoutes';
import Sidebar from './components/Sidebar/Sidebar';
import { getItems } from './store/items/operations';
import { useDispatch, useSelector } from 'react-redux';
import { TOGGLE_MODAL } from './store/modal/types';
import { SET_ITEM, SET_ITEMS } from './store/items/types';

export default function App() {

  const isActive = useSelector(state => state.modal.modal.isActive);
  const items = useSelector(state => state.items.items.data);
  const item = useSelector(state => state.items.item.data);

  const dispatch = useDispatch()

  const localStorageSetCart = () => {
    let cartArr = JSON.parse(localStorage.getItem("Cart")) || [];

    if (cartArr.includes(item.id)) {
      const newCartArr = cartArr.filter(el => el !== item.id);
      cartArr = newCartArr;
    } else {
      cartArr.push(item.id)
    }

    const cart = JSON.stringify(cartArr)
    localStorage.setItem("Cart", cart);
  }

  const addToCart = () => {
    const newArr = items.map(el => {
      console.log(item);
      console.log(el);
      if (el.id === item.id) {
        el.inCart = !el.inCart
      }
      return el
    })

    dispatch({type:SET_ITEMS,payload:newArr})
    dispatch({type:TOGGLE_MODAL,payload:false})

    localStorageSetCart();
  }

  useEffect(() => {
    dispatch(getItems())
  }, [dispatch])

  const openModal = (data) => {
    dispatch({type:SET_ITEM,payload:data});
    dispatch({type:TOGGLE_MODAL,payload:true})
  }

  const closeModal = () => {
    dispatch({type:TOGGLE_MODAL,payload:false})
  }

  const localStorageSetLiked = (item) => {
    let likedArr = JSON.parse(localStorage.getItem("Liked")) || [];

    if (likedArr.includes(item.id)) {
      const newlikedArr = likedArr.filter(el => el !== item.id);
      likedArr = newlikedArr;
    } else {
      likedArr.push(item.id)
    }

    const liked = JSON.stringify(likedArr)
    localStorage.setItem("Liked", liked);
  }

  const setLiked = (item) => {
    const newArr = items.map(el => {
      if (el.id === item.id) {
        el.isFavorite = !el.isFavorite
      }
      return el
    })
    dispatch({type:SET_ITEMS,payload:newArr})
    localStorageSetLiked(item)
  }
  return (
    <div className="App">
      <Sidebar/>
      <AppRoutes setLiked={setLiked} openModal={openModal} items={items}/>
      {isActive && <Modal header="Do you want to add this to cart?" text="" closeButton={true} closeModal={closeModal} addToCart={addToCart} />}
    </div>
  )
}