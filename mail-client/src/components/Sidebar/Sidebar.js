import React from 'react';
import { NavLink } from 'react-router-dom';
import './Sidebar.scss';

const Sidebar = () => {
  return (
    <div className="sidebar-pos">
      <ul className="sidebar">
        <li><NavLink className="sidebar-item" activeClassName="is-active" to='/main'>Main</NavLink></li>
        <li><NavLink className="sidebar-item" activeClassName="is-active" to='/cart'>Cart</NavLink></li>
        <li><NavLink className="sidebar-item" activeClassName="is-active" to='/favourites'>favourite</NavLink></li>
      </ul>
    </div>
  );
}

export default Sidebar;
