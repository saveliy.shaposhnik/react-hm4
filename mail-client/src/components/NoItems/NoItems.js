import React from 'react'
import "./NoItems.scss"

export default function NoItems() {
    return (
        <h2 className="no-items">
            No Items
        </h2>
    )
}
