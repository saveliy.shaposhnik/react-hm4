import "./Modal.scss";
import Button from "../Button/Button"
import React from 'react'

export default function Modal(props) {
    const { header, text, closeButton, closeModal, addToCart } = props;
    return (
        <>
            <div className="darkGround" onClick={closeModal}></div>
            <div className="modal">
                <header className="header">
                    {header}
                    {closeButton && <Button text="X" backgroundColor="inherit" onClick={closeModal} className="closeModalBtn" />}
                </header>
                <main className="main">
                    {text}
                    <div className="btns">
                        <Button backgroundColor="#523637" text="Ok" className="btn" onClick={addToCart} />
                        <Button backgroundColor="#523637" text="Cancel" className="btn" onClick={closeModal} />
                    </div>
                </main>
            </div>
        </>
    )
}


